/*
 * Copyright (C) 2018 ishaw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author ishaw
 */

public class BidList {
    private final Bid[] bidList;
    private Integer numberOfBids;
    
    public BidList(Integer bidListSize){
        this.numberOfBids = 0;
        this.bidList = new Bid[bidListSize];
    }
    
    public void addBid(Integer bidPrice, Buyer buyer){
        this.bidList[numberOfBids] = new Bid(bidPrice, buyer);
        this.numberOfBids++;
    }
    
    public Bid getBid(Integer i){
        return this.bidList[i];
    }
    
    public Integer getNumberOfBids(){
        return this.numberOfBids;
    }
    
    public Integer getBidListSize(){
        return bidList.length;
    }
    
    public void deleteBid(Buyer buyerToDelete){
        Boolean bidFound = false;
        for (Integer i = 0; i < numberOfBids; i++) {
            if (bidFound) {
                bidList[i - 1] = bidList[i];
                bidList[i] = null;
            } else {
                if (bidList[i].getBuyer().equals(buyerToDelete)) {
                    bidFound = true;
                    bidList[i] = null;
                }
            }
        }
        if (bidFound)
            this.numberOfBids--;
    }
    
    @Override
    public String toString(){
        String bidListDetails=new String();
        if(this.numberOfBids!=0){
            //bidListDetails+=String.format("");
            for(Integer i=0;i<this.numberOfBids;i++)
                bidListDetails += this.bidList[i].toString()+"\n";
        }
        else
            bidListDetails+="No bids have been registered on this market.";
        return bidListDetails;        
    }      
    
}
