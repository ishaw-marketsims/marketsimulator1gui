/*
 * Copyright (C) 2018 ishaw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author ishaw
 */

public class BuyerList {
    private final Buyer[] buyerList;
    private Integer numberOfBuyers;
    
    public BuyerList(Integer buyerListSize){
        this.numberOfBuyers = 0;
        this.buyerList = new Buyer[buyerListSize];
    }

    public void addBuyer(Integer MAX_PRICE, Integer demandUnits){
        this.buyerList[numberOfBuyers] = new Buyer(MAX_PRICE, demandUnits, numberOfBuyers);
        this.numberOfBuyers++;
    }
    
    public Integer getNumberOfBuyers(){
        return this.numberOfBuyers;
    }

    public Buyer getBuyer(Integer i){
        return this.buyerList[i];
    }

    @Override
    public String toString(){
        String agentListDetails=new String();
        if(this.numberOfBuyers!=0){
            agentListDetails+=String.format("%-12s%-13s%-13s\n","REFERENCE","MAX","DEMAND");
            for(Integer i=0;i<this.numberOfBuyers;i++)
                agentListDetails += this.buyerList[i].toString()+"\n";
        }
        else
            agentListDetails+="No buyers have been registered on this market.";
        return agentListDetails;        
    }      
    
}
