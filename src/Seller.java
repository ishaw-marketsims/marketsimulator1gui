/*
 * Copyright (C) 2018 ishaw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author ishaw
 */
public class Seller extends Agent {
    private final Integer MIN_PRICE;
    private Integer supplyUnits;
    private Integer moneyMade;
    
    public Seller(Integer MIN_PRICE, Integer supplyUnits, Integer numberOfSellers){
        super("S" + String.valueOf(numberOfSellers));
        this.MIN_PRICE = MIN_PRICE;
        this.supplyUnits = supplyUnits;
        this.moneyMade = 0;
    }
    
    public Integer bidPrice(){
        // bid price strategy - not the same, necessarily, as getMIN_PRICE
        return this.MIN_PRICE; // very desperate to sell
    }
    
    public Integer bidAmount(){
        // bid amount strategy - not the same, necessarily, as getSupplyUnits
        return this.supplyUnits; // trying to sell out
    }
    
    public Integer getSupplyUnits(){
        return this.supplyUnits;
    }
    
    public void setSupplyUnits(Integer units){
        this.supplyUnits = units;
    }
    
    public Integer getMoneyMade(){
        return this.moneyMade;
    }
    
    public void addMoneyMade(Integer money){
        this.moneyMade += money;
    }

    @Override
    public String getRef(){
        return super.getRef();
    }

    @Override
    public String toString(){
        String agentDetails = super.toString();
        agentDetails += String.format("%-13s%-16s%-17s","Min: " + MIN_PRICE,"Supply: " +supplyUnits,"Money Made: " + moneyMade);
        return agentDetails;
    }    
}
