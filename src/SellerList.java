/*
 * Copyright (C) 2018 ishaw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author ishaw
 */
public class SellerList {
    private final Seller[] sellerList;
    private Integer numberOfSellers;
    
    public SellerList(Integer sellerListSize){
        this.numberOfSellers = 0;
        this.sellerList = new Seller[sellerListSize];
    }

    public void addSeller(Integer MIN_PRICE, Integer supplyUnits){
        this.sellerList[numberOfSellers] = new Seller(MIN_PRICE, supplyUnits, numberOfSellers);
        this.numberOfSellers++;
    }
    
    public Integer getNumberOfSellers(){
        return this.numberOfSellers;
    }
    
    public Seller getSeller(Integer i){
        return this.sellerList[i];
    }
    
    public Integer length(){
        return sellerList.length;
    }
    
    @Override
    public String toString(){
        String agentListDetails=new String();
        if(this.numberOfSellers!=0){
            agentListDetails+=String.format("%-12s%-13s%-13s\n","REFERENCE","MIN","SUPPLY");
            for(Integer i=0;i<this.numberOfSellers;i++)
                agentListDetails += this.sellerList[i].toString()+"\n";
        }
        else
            agentListDetails+="No sellers have been registered on this market.";
        return agentListDetails;        
    }      
   
}
