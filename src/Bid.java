/*
 * Copyright (C) 2018 ishaw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author ishaw
 */

public class Bid {
    private Integer bidPrice;
    private Boolean bidAccepted;
    private final Buyer buyer;
    
    public Bid(Integer bidPrice, Buyer buyer){
        this.bidPrice = bidPrice;
        this.buyer = buyer;
        this.bidAccepted = false;
    }
    
    public Integer getBidPrice(){
        return this.bidPrice;
    }
    
    public void setBidPrice(Integer bidPrice){
        this.bidPrice = bidPrice;        
    }
    
    public Boolean getBidAccepted(){
        return this.bidAccepted;
    }
    
    public void setBidAccepted(Boolean bid){
        this.bidAccepted = bid;
    }
    
    public Buyer getBuyer(){
        return this.buyer;
    }
    
    @Override
    public String toString(){
        String bidDetails = "Bid Buyer Ref: "+this.buyer.getRef()+", Bid Price: "+this.bidPrice+", Bid Accepted: "+this.bidAccepted;
        return bidDetails;
    }
    
}
