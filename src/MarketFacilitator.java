/*
 * Copyright (C) 2018 ishaw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author ishaw
 */

public class MarketFacilitator {
    private final Integer LENGTH_OF_NEGOTIATION = 3; // number of steps in the negotiation
    private final BuyerList buyerList;
    private final SellerList sellerList;
    

    public MarketFacilitator(BuyerList buyerList, SellerList sellerList){
        this.buyerList = buyerList;
        this.sellerList = sellerList;
    }
     
    public String runNegotiation(){ // returns a big string report of what happened
        String report = new String();
        BidList bidList = new BidList(buyerList.getNumberOfBuyers());
        Integer numberOfActiveBuyers=0;
        Boolean noBidsAccepted = true; // compiler wants this initialised?
        // get bids from buyers, populate the bidList
        for(Integer i=0;i<bidList.getBidListSize();i++){
            if(buyerList.getBuyer(i).getDemandUnits() > 0) {//  if the buyer has any demand at all...
                bidList.addBid(buyerList.getBuyer(i).bidPrice(), buyerList.getBuyer(i));
                numberOfActiveBuyers++;
            report += "Added bid from Buyer "+buyerList.getBuyer(i).getRef()+" at asking price "+buyerList.getBuyer(i).bidPrice() + "\n";}
        }
        report += "Bids ready: "+sellerList.getNumberOfSellers()+" sellers, "+bidList.getNumberOfBids() + " bids.\n";
        
        // System.out.println(report);
        
        // begin the negotiation        
        for(Integer i=0;i<LENGTH_OF_NEGOTIATION;i++){
            report += "Round "+(i+1)+" of "+LENGTH_OF_NEGOTIATION+"\n";
            noBidsAccepted = true;
            
            // reset the remaining bids to not accepted
            for(Integer bid=0;bid<bidList.getNumberOfBids();bid++){
                bidList.getBid(bid).setBidAccepted(false);
            }
            
            // report += bidList; // this is for testing
            
            // each seller looks for a bid it can accept
            // if found, it accepts it, locking it for the rest of the round and fulfils as much demand as it can
            
            for(Integer j=0;j<sellerList.getNumberOfSellers();j++){                                     // for every seller
                if(sellerList.getSeller(j).getSupplyUnits() > 0){                                       // if the seller has supply left to sell
                    // report += "Checking seller "+sellerList.getSeller(j).getRef()+".\n";
                    for(Integer k=0;k<bidList.getNumberOfBids();k++){                                   // for every bid
                        if(!bidList.getBid(k).getBidAccepted()){                                        // if the bid hasn't already been accepted
                            if(sellerList.getSeller(j).bidPrice() < bidList.getBid(k).getBidPrice()     // and if the sales price is less than the buyer bid price
                                    && sellerList.getSeller(j).getSupplyUnits() > 0){                   // and another bidder hasn't bought out all the supply
                                bidList.getBid(k).setBidAccepted(true);                                 // accept the sale
                                noBidsAccepted = false;                                                 // something was accepted
                                Seller winningSeller = sellerList.getSeller(j);                         // (def to tidy up the code)
                                Buyer winningBuyer = bidList.getBid(k).getBuyer();                      // (def to tidy up the code)
                                Integer winningBidPrice = (winningSeller.bidPrice() + winningBuyer.bidPrice()) / 2;// set the unit price
                                Integer salesVolume;
                                if(winningSeller.bidAmount() <= winningBuyer.bidAmount()){              // if supply is less than or equals demand, sell out and reduce demand
                                    salesVolume = winningSeller.bidAmount();
                                    winningSeller.addMoneyMade(winningBidPrice * salesVolume);
                                    winningBuyer.addMoneySpent(winningBidPrice * salesVolume);
                                    winningBuyer.setDemandUnits(winningBuyer.bidAmount() - winningSeller.bidAmount());
                                    winningSeller.setSupplyUnits(0);
                                } else {                                                                // supply is more than demand, fully supply and reduce available supply left
                                    salesVolume = winningBuyer.bidAmount();
                                    winningSeller.addMoneyMade(winningBidPrice * salesVolume);
                                    winningBuyer.addMoneySpent(winningBidPrice * salesVolume);
                                    winningSeller.setSupplyUnits(winningSeller.bidAmount() - winningBuyer.bidAmount());
                                    winningBuyer.setDemandUnits(0);
                                    bidList.deleteBid(winningBuyer);
                                }
                                report += "Buyer "+winningBuyer.getRef()+" buys "+salesVolume+" from Seller "+winningSeller.getRef()+" at winning price: "+winningBidPrice+"\n";
                            }
                        }
                    }
                }
            }
        if(noBidsAccepted)
            report += "No bids accepted this round.\n";
        }
    return report;
    }
}
