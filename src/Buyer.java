/*
 * Copyright (C) 2018 ishaw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author ishaw
 */
public class Buyer extends Agent {
    private final Integer MAX_PRICE;
    private Integer demandUnits;
    private Integer moneySpent;
    
    public Buyer(Integer MAX_PRICE, Integer demandUnits, Integer numberOfBuyers){
        super("B" + String.valueOf(numberOfBuyers));
        this.MAX_PRICE = MAX_PRICE;
        this.demandUnits = demandUnits;
        this.moneySpent = 0;
    }

    public Integer bidPrice(){
        // bid price strategy - not the same, necessarily, as getMAX_PRICE
        return this.MAX_PRICE; // very desperate to buy
    }
    
    public Integer bidAmount(){
        // bid amount strategy - not the same, necessarily, as getDemandUnits
        return this.demandUnits; // trying to satisfy all demand
    }
    
    public Integer getDemandUnits(){
        return this.demandUnits;
    }
    
    public void setDemandUnits(Integer units){
        this.demandUnits = units;
    }
    
    public Integer getMoneySpent(){
        return this.moneySpent;
    }
    
    public void addMoneySpent(Integer money){
        this.moneySpent += money;
    }
    
    @Override
    public String getRef(){
        return super.getRef();
    }
    
    @Override
    public String toString(){
        String agentDetails = super.toString();
        agentDetails += String.format("%-13s%-16s%-17s","Max: " + MAX_PRICE,"Demand: " + demandUnits,"Money Spent: " + moneySpent);
        return agentDetails;
    }    
}
